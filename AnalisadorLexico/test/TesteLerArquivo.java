import analisadorlexico.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class TesteLerArquivo {
    public static void main(String[] args) {
        String caminhoArquivo = "Digite o caminho do arquivo a ser lido";
        lerArquivo(caminhoArquivo);
    }
    
    /**
     * 
     * Função que ler o texto a partir de um arquivo informado.
     */
    public static void lerArquivo(String caminhoArquivo){
        String linha = "";
        try{
            BufferedReader br = new BufferedReader(new FileReader(caminhoArquivo));
            while(br.ready()){
		linha = linha + br.readLine() + "\n";
            }
            br.close();
	}catch(IOException ioe){
            ioe.printStackTrace();
	}
        analisar(linha);
    }
    
    /**
     * 
     * Função que invoca o método lexer da classe Lexer.
     * @see analisadorlexico.Lexer
     * @param texto  
     */
    public static void analisar(String texto){
        Lexer lexer = new Lexer();
        ArrayList<Token> resultado = lexer.lex(texto);
        String r = "";
        for(Token token : resultado){
            System.out.println(token);
            r = r + token + "\n";
        }
    }
}