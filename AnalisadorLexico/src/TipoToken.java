package analisadorlexico;
/**
 *
 * Classe para definir os padrões dos lexemas. Defina nesta classe as constantes com a string regex caso deseje um novo tipo de token da sua linguagem.
 * 
 * 
 * @author Michael Charles
 * @see analisadorlexico.Token
 * @since V1.0
 */

public enum TipoToken{
    
    /**
     * Constantes de Regex. 
     */
    FLOAT("[0-9]+(.)[0-9]+"), NUMBER("-?[0-9]+"), BINARYOP("[*|/|+|-|=]"), RELATIONALOP("==|>|<|>=|<=|!="), SIMBOL("[;|:|{|}|(|)|,]"), WHITESPACE("[ \t\f\r\n]+"), RESERVEDWORD("int|float|string|return|break|print|forloop|if|else|elseif"), ID("[a-z]([a-z]|[A-Z]|[0-9])*"), STRING("\"([a-z]|[A-Z]|[0-9]|[ \t\f\r\n])*\"");

    /**
     * String de cada Regex de cada lexema definido.
     */
    public final String pattern;

    /**
     * 
     * Define o regex da classe. Veja na classe Lexer como esse construtor é utilizado.
     * 
     * @param pattern 
     */
    private TipoToken(String pattern){
        this.pattern = pattern;
    }
}

