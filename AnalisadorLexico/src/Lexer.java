package analisadorlexico;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * A classe com os métodos do analisador léxico. 
 * 
 * 
 * @author Michael Charles
 * @see analisadorlexico.Token
 * @see analisadorlexico.TipoToken
 * @since V1.0
 */
public class Lexer {
    
    /**
     * 
     * Realiza a análise de uma string. É feita a concatenação de strings regex para comparação única com o texto de entrada.
     * 
     * @param texto
     * @return 
     *       Um ArrayList de Tokens.
     */
    public static ArrayList<Token> lex(String texto){
        ArrayList<Token> tokens = new ArrayList<Token>();
        
        StringBuffer tokenPatternsBuffer = new StringBuffer();
        for(TipoToken tokenType : TipoToken.values()){
            tokenPatternsBuffer.append(String.format("|(?<%s>%s)", tokenType.name(), tokenType.pattern));
        }
        Pattern tokenPatterns = Pattern.compile(new String(tokenPatternsBuffer.substring(1)));
        
        Matcher matcher = tokenPatterns.matcher(texto);
        while(matcher.find()){
            if(matcher.group(TipoToken.NUMBER.name()) != null){
                tokens.add(new Token(TipoToken.NUMBER, matcher.group(TipoToken.NUMBER.name())));
                continue;
            }else if(matcher.group(TipoToken.FLOAT.name()) != null){
                tokens.add(new Token(TipoToken.FLOAT, matcher.group(TipoToken.FLOAT.name())));
                continue;
            }else if(matcher.group(TipoToken.BINARYOP.name()) != null){
                tokens.add(new Token(TipoToken.BINARYOP, matcher.group(TipoToken.BINARYOP.name())));
                continue;
            }else if(matcher.group(TipoToken.RESERVEDWORD.name()) != null){
                tokens.add(new Token(TipoToken.RESERVEDWORD, matcher.group(TipoToken.RESERVEDWORD.name())));
                continue;
            }else if(matcher.group(TipoToken.ID.name()) != null){
                tokens.add(new Token(TipoToken.ID, matcher.group(TipoToken.ID.name())));
                continue;
            }else if(matcher.group(TipoToken.STRING.name()) != null){
                tokens.add(new Token(TipoToken.STRING, matcher.group(TipoToken.STRING.name())));
                continue;
            }else if(matcher.group(TipoToken.SIMBOL.name()) != null){
                tokens.add(new Token(TipoToken.SIMBOL, matcher.group(TipoToken.SIMBOL.name())));
                continue;
            }else if(matcher.group(TipoToken.RELATIONALOP.name()) != null){
                tokens.add(new Token(TipoToken.RELATIONALOP, matcher.group(TipoToken.RELATIONALOP.name())));
                continue;
            }else if(matcher.group(TipoToken.WHITESPACE.name()) != null){
                continue;
            }
        }
        
        return tokens;
    }
}