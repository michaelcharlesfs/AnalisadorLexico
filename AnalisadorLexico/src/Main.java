package analisadorlexico;

import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Main {
    public static void main(String[] args) {
        menu();
    }
    
    /**
     * 
     * Carrega a pop-up de menu. 
     */
    public static void menu(){
        JTextArea textArea = new JTextArea("1 - Digitar código \n2 - Ler um arquivo \n3 - Sair");
        JScrollPane scrollPane = new JScrollPane(textArea);  
        textArea.setLineWrap(true);  
        textArea.setWrapStyleWord(true); 
        scrollPane.setPreferredSize( new Dimension( 500, 200 ) );
        String opcao = JOptionPane.showInputDialog(null, scrollPane, "Resultado da análise", JOptionPane.YES_NO_OPTION);
        
        if(opcao == "1"){
            System.out.println("Digitar código");
            digitarCodigo();
        }else if(opcao == "2"){
            System.out.println("Ler arquivo");
            lerArquivo();
        }else if(opcao == "3"){
            System.out.println("Sair do programa");
            System.exit(0);
        }else{
            System.out.println("Opção inválida!");
            System.exit(0);
        }
        
    }
    
    /**
     * 
     * Função que recebe o texto por entrada do teclado. 
     */
    public static void digitarCodigo(){
        JTextArea textArea = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(textArea);  
        textArea.setLineWrap(true);  
        textArea.setWrapStyleWord(true); 
        scrollPane.setPreferredSize( new Dimension( 500, 500 ) );
        JOptionPane.showMessageDialog(null, scrollPane, "Código a ser analisado", JOptionPane.YES_NO_OPTION);
        analisar(textArea.getText());
        menu();
    }
    
    /**
     * 
     * Função que ler o texto a partir de um arquivo informado.
     */
    public static void lerArquivo(){
        String linha = "";
        try{
            String nome = "";
            nome = JOptionPane.showInputDialog(null, "Digite o caminho do arquivo");
            BufferedReader br = new BufferedReader(new FileReader(nome));
            while(br.ready()){
		linha = linha + br.readLine() + "\n";
            }
            br.close();
	}catch(IOException ioe){
            ioe.printStackTrace();
	}
        analisar(linha);
        menu();
    }
    
    /**
     * 
     * Função que invoca o método lexer da classe Lexer.
     * @see analisadorlexico.Lexer
     * @param texto  
     */
    public static void analisar(String texto){
        Lexer lexer = new Lexer();
        ArrayList<Token> resultado = lexer.lex(texto);
        String r = "";
        for(Token token : resultado){
            System.out.println(token);
            r = r + token + "\n";
        }
        JTextArea textArea = new JTextArea(r);
        JScrollPane scrollPane = new JScrollPane(textArea);  
        textArea.setLineWrap(true);  
        textArea.setWrapStyleWord(true); 
        scrollPane.setPreferredSize( new Dimension( 500, 500 ) );
        JOptionPane.showMessageDialog(null, scrollPane, "Resultado da análise", JOptionPane.YES_NO_OPTION);
    }
}