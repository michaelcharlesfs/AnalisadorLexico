package analisadorlexico;

/**
 *
 * Classe usada para separar e classificar os conjuntos de caraceteres de um string para identificar lexemas e separá-los em tokens. 
 * 
 * @author Michael Charles
 * @see analisadorlexico.TipoToken
 * @since V1.0
 */

public class Token {
    
    /**
     * 
     * Lexema.
     */
    public TipoToken type;
    /**
     * 
     * String do token.
     */
    public String data;

    /**
     * 
     * Construtor da classe token.
     * 
     * @param type
     *        Lexema.  
     * @param data 
     *        String do token.
     */
    public Token(TipoToken type, String data){
        this.type = type;
        this.data = data;
    }

    /**
     * 
     * Override do método da classe String para definir padrão de retorno da classe.
     * 
     * @return 
     *  Retorna os atributos da classe
     */
    @Override
    public String toString(){
        return String.format("(%s %s)", type.name(), data);
    }
    
}
