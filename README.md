Este analisador léxico foi construído para trabalho de conclusão da disciplina de compiladores do curso de Bacharelado em Ciência da Computação pela Universidade do Estado do Rio Grande do Norte (UERN).

O código foi implementado pelos discentes:
	- Michael Charles
	- Daniel Alves
	- André Noberto
	- João Neto

O código foi desenvolvido com base no modelo de Gio Carlo Cielo. O modelo pode ser encontrado no blog pessoal do mesmo onde ele descreve no tutorial todo o esquema lógico do analisador léxico.

Fonte: http://www.giocc.com/writing-a-lexer-in-java-1-7-using-regex-named-capturing-groups.html

1 - Como usar

Para executar a aplicação, acesse a pasta dist e execute o arquivo .jar.

Caminho: analisadorlexico/dist/AnalisadorLexico.jar

2 - Execute e compile na sua IDE

Baixe o código fonte e execute a classe main do pacote analisadorlexico

